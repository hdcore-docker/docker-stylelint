# HDCore - docker-stylelint

## Introduction

This is a small container image that contains a Node environment with stylelint. This container is very usefull for automatic testing during CI.

## Image usage

- Run stylelint:

```bash
docker run --rm -v /path/to/code:/code hdcore/docker-stylelint:<version> stylelint.sh <arguments>
```

- Run shell:

```bash
docker run -it --rm -v /path/to/code:/code hdcore/docker-stylelint:<version> /bin/sh
```

- Use in .gitlab-ci.yml:

```bash
image: hdcore/docker-stylelint:<version>
script: stylelint.sh <arguments>
```

## Available tags

- hdcore/docker-stylelint:16

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-stylelint
  - registry.hub.docker.com/hdcore/docker-stylelint
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-stylelint

## Building image

Build:

```bash
docker compose build local-stylelint-16
```

## Running the image

### Default

```bash
docker compose run --rm local-stylelint-16
```
