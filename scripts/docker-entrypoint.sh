#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2023-12-28 11:50

# docker-entrypoint.sh
# for hdcore docker-stylelint image

CGREEN="\e[32m"
CNORMAL="\e[0m"

# shellcheck disable=SC2059
printf "== ${CGREEN}Start docker-entrypoint.sh ${0}${CNORMAL} ==\n"

printf "HDCore docker-stylelint container image\n"
printf "Installed version of stylelint:\n"
stylelint --version

# shellcheck disable=SC2059
printf "== ${CGREEN}End docker-entrypoint.sh ${0}${CNORMAL} ==\n"

# Execute docker CMD
exec "$@"
