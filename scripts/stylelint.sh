#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2023-08-28 16:45

# docker-entrypoint.sh
# for hdcore docker-stylelint image

if [ -z "${1}" ]
then
    echo "Error: no parameter given!"
    exit 99
fi

# shellcheck disable=SC2068 # double quote not allowed due to extra parameter expansion
stylelint $@
